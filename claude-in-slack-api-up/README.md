# claude-in-slack-api

通过 Slack API 来使用 Claude

`.env.template` 重命名为 `.env` 并填入 Slack APP Token 和 Claude Bot ID

## 运行

```bash
pip install -r requirements.txt

python claude.py
```

调用接口文档地址：[http://127.0.0.1:8088/docs](http://127.0.0.1:8088/docs)

## 对话

```curl
curl -X 'POST' \
  'http://127.0.0.1:8088/claude/chat' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "prompt": "你好啊"
}'
```

## 清除上下文

```curl
curl -X 'POST' \
  'http://127.0.0.1:8088/claude/reset' \
  -H 'accept: application/json' \
  -d ''
```

## 公众号文章

[Claude｜媲美ChatGPT，如何免费接入到个人服务](https://mp.weixin.qq.com/s?__biz=Mzg4MjkzMzc1Mg==&mid=2247483961&idx=1&sn=c009f4ea28287daeaa4de17278c8228e&chksm=cf4e68aef839e1b8fe49110341e2a557e0b118fee82d490143656a12c7f85bdd4ef6f65ffd16&token=1094126126&lang=zh_CN#rd)




curl -X 'POST' \
'http://127.0.0.1:8088/claude/chat' \
-H 'accept: application/json' \
-H 'Content-Type: application/json' \
-H 'x-token: 13832bf5-ed7c-4b5c-a30b-e7db48d25b86' \
-d '{
"prompt": "你好啊"
}'

curl -X 'POST' \
'http://127.0.0.1:8088/claude/agent' \
-H 'accept: application/json' \
-H 'Content-Type: application/json' \
-H 'x-token: 13832bf5-ed7c-4b5c-a30b-e7db48d25b86' \
-d '{
"prompt": "刘世仲是贵州茅台的管理层吗?"
}'


curl -X 'POST' \
'http://127.0.0.1:8088/claude/chat' \
-H 'accept: application/json' \
-H 'Content-Type: application/json' \
-H 'x-token: 13832bf5-ed7c-4b5c-a30b-e7db48d25b86' \
-d '{
"prompt": "our database as 2 tables, the table structures are listed as follow:

1. table name: 公司管理层信息
columns:
- `index`: id of the row
- `股票代码`: the unique code of a stock
- `股票名称`: the name of a stock
- `公告日期`: the date of 公司管理层信息 is announced
- `姓名`: the last name and first name of 公司管理层 which is a person
- `性别`: the gender of 公司管理层 which is a person
- `岗位类别`: the position in board of 公司管理层 which is a person
- `岗位`: the position in company of 公司管理层 which is a person
- `学历`: the education of 公司管理层 which is a person
- `国籍`: the nationality of 公司管理层 which is a person
- `个人简历`: the resume of 公司管理层 which is a person

2. table name: 投资者互动表
columns:
- `index`: id of the row
- `日期`: the announcement date of 投资者互动 which is questions and answers between 投资者 and 董秘
- `股票代码`: the unique code of a stock
- `股票名称`: the name of a stock
- `投资者互动内容`: questions and answers between 投资者 and 董秘
- `概念板块`: 板块 or 概念板块 is a group of stocks shares something in common

3. table name: 概念板块与股票的对应关系表
columns:
- `index`: id of the row
- `股票代码`: the unique code of a stock
- `股票名称`: the name of a stock
- `概念板块`: 板块 or 概念板块 is a group of stocks shares something in common


4. table name: 概念板块与股票的对应关系表
columns:
- `index`: id of the row
- `概念板块`: 板块 or 概念板块 is a group of stocks shares something in common
- `主力净流入`: 大单买入成交额 - 大单卖出成交额
- `主力净流入最大股票`: the name of stock with gets most attention in 概念板块

5. table name: 股票基本信息
columns:
- `index`: id of the row
- `股票代码`: the unique code of a stock
- `股票名称`: the name of a stock
- `所属行业`: the industry with the stock is belonging to
- `拼音缩写`: 拼音 initials which is a type unique index of the stock
- `是否沪深港通标的`: 沪港通 means the stock can be traded from hongkong by shanghai stock exchange. 深港通 means the stock can be traded from hongkong by shenzhen stock exchange. empty string mean the stock cannot be traded from hongkong

6. table name: 股票热度排名表
columns:
- `index`: id of the row
- `股票代码`: the unique code of a stock
- `股票名称`: the name of a stock
- `热度`: the number of user who has browsed the stock today. it indicates how hot the stock is.
- `热度变化量`: change of the 热度 from yesterday to today
- `涨幅`: change of price of the stock


7. table name: 雪球
columns:
- `index`: id of the row
- `用户名`: the unique name of a user in 雪球 forum
- `发帖时间`: the time when 用户名 make a post
- `点赞数`: the number of other users who like the post
- `收藏数`: the number of other users who archive the post
- `回复数`: the number of other users who reply the post
- `转发数`: the number of other users who re-post the post
- `发帖内容`: the text content of the post. the content may or may not mention few stocks by their names


according to user question, please tell me which table I can find the answer
only answer me with one word of the table name. do not tell me the reason
user question: 雪球上的轮回666今天说了啥"
}'