# !/usr/bin/env python3
"""
==== No Bugs in code, just some Random Unexpected FEATURES ====
┌─────────────────────────────────────────────────────────────┐
│┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐│
││Esc│!1 │@2 │#3 │$4 │%5 │^6 │&7 │*8 │(9 │)0 │_- │+= │|\ │`~ ││
│├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┤│
││ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{[ │}] │ BS  ││
│├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤│
││ Ctrl │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  ││
│├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────┬───┤│
││ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│Shift │Fn ││
│└─────┬──┴┬──┴──┬┴───┴───┴───┴───┴───┴──┬┴───┴┬──┴┬─────┴───┘│
│      │Fn │ Alt │         Space         │ Alt │Win│   HHKB   │
│      └───┴─────┴───────────────────────┴─────┴───┘          │
└─────────────────────────────────────────────────────────────┘

自定义工具类。

Author: pankeyu
Date: 2023/05/03
"""
import os
os.environ["OPENAI_API_KEY"] = "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"

import datetime
import pandas as pd
from langchain.tools import Tool
from langchain.llms import OpenAI

from config import (
    MAX_ACCOUNT_MESSAGE_LEN, 
    FIND_DB_TABLE_PROMPT_TEMPLATE,
    DATABASE_STRUCTURE,
    FIND_INFO_PROMPT_PATTERN
)

from client.mysql_client import MySQLClient

from lixinger.query import *

my_sql_client = MySQLClient()
message_df = pd.read_csv('/workspace/wechat-chatgpt/ChatStockMY/data/data3_雪球帖子.csv')
message_df['发帖时间'] = pd.to_datetime(message_df['发帖时间'])

rank_df = pd.read_csv('/workspace/wechat-chatgpt/ChatStockMY/data/data2_热度历史数据.csv')
rank_df['交易日期'] = pd.to_datetime(rank_df['交易日期'])


def find_stock_rank(
        stock_name: str 
       
    ):
    """
    通过调用数据库接口找到对应股票当前的热度排名。

    Args:
        stock_name (str): 股票名称, e.g. -> 贵州茅台
        }
    """
    #     all_stock=stock_name.split("、").split(",");
    # if len(all_stock) ==1:
    #     stock_df = rank_df[rank_df['股票名称'] == all_stock]
    # else:
    #     for x in all_stock:
    #         if stock_df ==None:
    #             stock_df = rank_df[rank_df['股票名称'] == x]
    #         else:
    #             stock_df.append(rank_df[rank_df['股票名称'] == x])
    stock_df = rank_df[rank_df['股票名称'] == stock_name]

    if stock_df.empty:
        rank_df[rank_df['股票代码'] == stock_name]

    if stock_df.empty:
        return '未能查找到指定的股票，我应该直接返回 Observation: 未知'
    
    stock_df = stock_df.set_index('交易日期')

    select_day = datetime.date.today() - datetime.timedelta(days=1)
    select_day = select_day.strftime('%Y-%m-%d')

    select_day = '2022-05-04'                                            # 先写死，取5月4日的信息

    latest_rank_df = stock_df.sort_index().loc[select_day:, :] 
    ranks = latest_rank_df['排名'].tolist()
    print('find ranks:', ranks)

    if not ranks:
        return '未能查找到指定的股票，我应该直接返回 Observation: 未知'
    
    return ranks[-1]


tool_of_find_stock_rank = Tool.from_function(
    func=find_stock_rank,
    name='查询股票热度排名',
    description='当需要查找某只股票的热度排名时调用的函数，函数的输入应当是一个股票（公司）的具体名字。'
)

def find_account_message(
        account_name: str        
    ):
    """
    通过调用数据库接口找到对应对应大v用户的推荐股票。

    Args:
        account_name (str): 股票名称, e.g. -> 贵州茅台
        }
    """
    user_message_df =  message_df[message_df['用户名'] == account_name]
    if user_message_df.empty:
        user_message_df =message_df
        # return '未能查找到指定用户的言论或观点，我应该直接返回 Observation: 未知'
    
    user_message_df = user_message_df.set_index('发帖时间')
    latest_message_df = user_message_df.sort_index()[::-1]              # 按照时间顺序做倒排，越近的时间越优先使用

    if latest_message_df.empty:
        return '未能查找到指定用户的言论或观点，我应该直接返回 Observation: 未知'

    account_messages = ''
    for i, (_time, row) in enumerate(latest_message_df.iterrows()):
        account_messages += f'发帖{i+1}(发帖时间: {_time}): {row["发帖内容"]}\n---\n'
    
    return account_messages[:MAX_ACCOUNT_MESSAGE_LEN]


tool_of_find_account_message = Tool.from_function(
    func=find_account_message,
    name='查询指定用户看法',
    description='当需要查询某个人对某个板块或某只股票的观点时所使用的函数，函数的输入应当是一个具体人物的名字（或网名）。'
)


def select_information_from_database(query: str):
    """
    将用户query变成sql语句，并从数据库中找到答案。

    Args:
        query (str): _description_
    """
    query = query + '？' if not query.endswith('？') else query
    
    current_llm = OpenAI(temperature=0)
    message_return_when_sql_failed = 'Can not find such table in database, observation should be: '\
                                'No relavent informations, ' \
                                'now I get my final answer: Can not find data according to your question.'

    table_descriptions = [f'table name: {key}\ncolumns:\n{value}' for key, value in DATABASE_STRUCTURE.items()]
    table_description = '\n\n'.join(table_descriptions)
    find_table_name_prompt = FIND_DB_TABLE_PROMPT_TEMPLATE.format(
        table_num = len(DATABASE_STRUCTURE),
        table_description=table_description,
        question=query
    )
    print('【Fine Database Name Prompt】\n', find_table_name_prompt)
    table_name = current_llm(find_table_name_prompt).strip()
    print('【Table Name from ChatGPT】 >>> ', table_name)

    if table_name not in DATABASE_STRUCTURE:
        return message_return_when_sql_failed
    
    table_description = f'table name: {table_name}\ncolumns:\n{DATABASE_STRUCTURE[table_name]}'
    find_information_prompt = FIND_INFO_PROMPT_PATTERN.format(
        question=query,
        table_description=table_description
    )
    print('【Fine Information Prompt】 \n', find_information_prompt)
    sql = current_llm(find_information_prompt).strip()
    print('【SQL from ChatGPT】 >>> ', sql)

    sql_res = my_sql_client.excute_sql(sql)
    sql_res = sql_res if sql_res else message_return_when_sql_failed
    return f'{sql_res}'[:MAX_ACCOUNT_MESSAGE_LEN]


tool_of_select_information_from_database = Tool.from_function(
    func=select_information_from_database,
    name='search in database',
    # description="useful for when you need to search something in database, "\
    #             "action input should be user's origin chinese question."
    description="当你需要查找数据库时所用的工具，Action Input应当为用户最开始的问题，请不要做任何改写。"
)
def find_macro_population(query: str):
    rlt = query_json('macro/population', {
	"token": "ffad9101-8689-4b5d-bd79-763c58522a95",
	"areaCode": "cn",
	"startDate": "2022-05-13",
	"endDate": "2023-05-16",
	"metricsList": [
		"y.tp.t"
	]
    })
    return rlt["data"][0]["y"]["tp"]

tool_of_find_macro_population = Tool.from_function(
    func=find_macro_population,
    name='查询中国人口数据',
    # description="useful for when you need to search something in database, "\
    #             "action input should be user's origin chinese question."
    description="当需要查询中国总的人口所使用的函数，函数没有任何输入"
)


if __name__ == '__main__':
    from rich import print

    # res = find_account_message(
    #     'sst金狐狸'
    # )
    # print(res)

    # res = find_stock_rank(
    #     '平安银行'
    # )
    # print(res)

    # res = select_information_from_database(
    #     # "剑桥科技的行业是什么？"
    #     '平安银行'
    # )
    # print(res)
    r =find_macro_population()
    print(r)