import os
import torch.cuda


EMBEDDING_MODEL = "GanymedeNil/text2vec-large-chinese"
EMBEDDING_DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"

CHUNK_SIZE = 500        # 匹配后单段上下文长度