# -*- coding: utf-8 -*-
import pandas as pd
import tushare as ts
import time
import datetime
from sqlalchemy import create_engine
import os

ts.set_token('37d5b9baef929464347ddaa08240f1179ffe4af894faf7b14d8b3b5c')


def get_stock_info():
    file_path = '/root/ChatStock/data/股票代码与股票名称.csv'

    if os.path.exists(file_path):
        stock_codes =pd.read_csv(file_path)
    else:
        # 获取股票代码与股票名称的对应
        stock_codes = ts.pro_api().stock_basic(exchange='SSE', list_status='L', fields='ts_code,name')
        s = ts.pro_api().stock_basic(exchange='SZSE', list_status='L', fields='ts_code,name')
        stock_codes = pd.concat([stock_codes, s], axis=0)
        stock_codes = stock_codes.sort_values(by=['ts_code']).reset_index(drop=True)
        stock_codes.columns = ['股票代码', '股票名称']
        stock_codes.to_csv(file_path, encoding='utf-8', index=False)
    return stock_codes


def get_stk_managers(stock_code: str):
    file_path = '/root/ChatStock/data/data1_'+stock_code+'公司管理层履历.csv'
    if os.path.exists(file_path):
        df =pd.read_csv(file_path)
    else:
        # 获取指定股票代码的公司高管信息
        df = ts.pro_api().stk_managers(ts_code=stock_code, fields='ts_code,ann_date,name,gender,lev,title,edu,national,birthday,begin_date,end_date,resume')
        df.columns = ['股票代码', '公告日期', '姓名', '性别', '岗位类别', '岗位', '学历', '国籍', '出生日期', '上任日期', '离任日期', '个人简历']
        df.loc[df['性别'] == 'M', '性别'] = '男'
        df.loc[df['性别'] == 'F', '性别'] = '女'
        df = df.loc[df['离任日期'].isnull() ]
    return df


if __name__ == '__main__':
    # pd.set_option('max_columns', None)
    pd.set_option('display.unicode.ambiguous_as_wide', True)
    pd.set_option('display.unicode.east_asian_width', True)
    pd.set_option('display.width', 1000)

    print(get_stock_info())
    print(get_stk_managers('688799.SH'))
    get_stk_managers('688799.SH').to_csv('/root/ChatStock/data/data1_公司管理层履历.csv', encoding='utf-8', index=False)

    # 1.帮我总结下公司管理层人物经历
    #
    # 	页面交互：
    # 		用户输入股票代码，跑接口获取数据，给GPT，完了展示总结内容即可
    #
    # 	python:
    # 		code1_公司管理层履历.py 里面给出2个function：输出所有股票代码和股票名称的对应、输入股票代码输出管理层的全部信息
    # 		获取到的信息全部输入GPT让他总结即可
    #
    # 	prompt:
    # 		Now pretend you are expert making summary of long text.
    # 		Answer me with short summary in Chinese.
    # 		Summarize 姓名, 个人简历 of the following long text
    # 		long text:
    # 			...



