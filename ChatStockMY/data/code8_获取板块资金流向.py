# -*- coding: utf-8 -*-
import tushare as ts
import pandas as pd
import akshare as ak
from sqlalchemy import create_engine

import os
from datetime import datetime


    
if __name__ == '__main__':
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    results = ak.stock_sector_fund_flow_rank(indicator="今日", sector_type="概念资金流")
    results = results[['名称', '今日主力净流入-净额', '今日主力净流入最大股']]
    results.columns = ['概念板块', '主力净流入', '主力净流入最大股票']
    print(results)
    
    source_file = '/root/ChatStock/data/data8_获取板块资金流向.csv'  # 替换为源文件的路径

    if os.path.exists(source_file):
        file_name, file_extension = os.path.splitext(source_file)
        current_date = datetime.now().strftime("%Y%m%d_%H%M%S")
        destination_file = f"{file_name}_{current_date}{file_extension}"
        
        os.rename(source_file, destination_file)
    
    results.to_csv(source_file, encoding='utf-8', index=False, errors='ignore')
