import json
import random
import re
import time
import pandas as pd
import requests
from feapder.utils.log import log
from feapder.network.selector import Selector
from feapder.network.user_agent import get as get_ua
import datetime
from sqlalchemy import create_engine
import os
from datetime import date


def remove_emoji(text):
    emoji_pattern = re.compile("["
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               "]+", flags=re.UNICODE)
    return emoji_pattern.sub(r'', text)


def get_user_info(user_id):
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
                  ",application/signed-exchange;v=b3;q=0.7",
        "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "Pragma": "no-cache",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "none",
        "Sec-Fetch-User": "?1",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": get_ua('chrome'),
        "sec-ch-ua": "\"Chromium\";v=\"112\", \"Microsoft Edge\";v=\"112\", \"Not:A-Brand\";v=\"99\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\""
    }
    url = f"https://xueqiu.com/u/{user_id}"
    try:
        _ = session.get(url, headers=headers, timeout=3)
    except Exception as e:
        log.error(str(e))
        return get_user_info(user_id)
    return '查询成功'


def download_inside_info(link):
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng"
                  ",*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
        "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "none",
        "Sec-Fetch-User": "?1",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": get_ua("chrome"),
    }
    response = session.get(link, headers=headers)
    content = re.search(r'window\.SNOWMAN_STATUS = (.*?);\W*window\.SNOWMAN_TARGET', response.text, re.S | re.I).group(
        1)
    text = json.loads(content).get("text")
    return text


def get_user_comment(user_id, page=1):
    global results
    log.info(f"now is download {user_id} {page} 页数据")
    url = "https://xueqiu.com/v4/statuses/user_timeline.json"
    params = {
        "page": f"{page}",
        "user_id": f"{user_id}",
        "type": 0,
    }
    headers = {
        "Accept": "*/*",
        "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "Referer": f"https://xueqiu.com/u/{user_id}",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin",
        "User-Agent": get_ua('chrome'),
        "X-Requested-With": "XMLHttpRequest",
    }

    dr = re.compile(r'<[^>]+>', re.S)
    response = session.get(url, headers=headers, params=params)
    statuses = response.json().get("statuses")
    if statuses:
        for i in statuses:
            # print(i)
            statuses_user_name = i.get("user").get("screen_name")
            statuses_created_at = datetime.datetime.fromtimestamp(int(i.get("created_at")) / 1000)
            statuses_retweet_count = i.get("retweet_count")
            statuses_reply_count = i.get("reply_count")
            statuses_fav_count = i.get("fav_count")
            statuses_target = i.get("target")
            statuses_like_count = i.get("like_count")
            statuses_text = dr.sub('', i.get("text").replace('<br/>', '/n'))
            statuses_title = i.get("title")
            statuses_content = ""
            if statuses_title != "":
                link = "https://xueqiu.com" + statuses_target
                statuses_content = dr.sub('', download_inside_info(link).replace('<br/>', '\n'))
            text = statuses_text
            if not text:
                text = statuses_title + '\n' + statuses_content
            j = {
                "用户名": statuses_user_name,
                "发帖时间": statuses_created_at,
                "点赞数": statuses_like_count,
                "收藏数": statuses_fav_count,
                "回复数": statuses_reply_count,
                "转发数": statuses_retweet_count,
                "发帖内容": remove_emoji(text),
            }
            results = pd.concat([results, pd.DataFrame.from_dict(j, orient='index').T], axis=0)
            print(j)
    maxPage = response.json().get("maxPage")
    if maxPage and page < maxPage:
        time.sleep(random.randint(3, 7))
        return get_user_comment(user_id, page + 1)
    return "入库成功"


if __name__ == '__main__':
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    results = pd.DataFrame()
    session = requests.session()
    user_ids = ['8282709675', '7329341148', '4299879533', '1593782580', '6186913084', '3849856324', '2292705444']
    for id in user_ids:
        get_user_info(user_id=id)
        get_user_comment(user_id=id)
    print(results)
    
    source_file='/root/ChatStock/data/data7_雪球帖子.csv'
    if os.path.exists(source_file):
        file_name, file_extension = os.path.splitext(source_file)
        current_date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        destination_file = f"{file_name}_{current_date}{file_extension}"
        
        os.rename(source_file, destination_file)
    results.to_csv(source_file, encoding='utf-8', index=False, errors='ignore')

