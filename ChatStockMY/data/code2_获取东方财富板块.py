# -*- coding: utf-8 -*-
import pandas as pd
import akshare as ak
import tushare as ts
import time
import datetime


if __name__ == '__main__':
    # pd.set_option('max_columns', None)
    pd.set_option('display.unicode.ambiguous_as_wide', True)
    pd.set_option('display.unicode.east_asian_width', True)
    pd.set_option('display.width', 1000)

    ts.set_token('37d5b9baef929464347ddaa08240f1179ffe4af894faf7b14d8b3b5c')
    codes = ts.pro_api().query('stock_basic', exchange='', list_status='L', fields='ts_code,symbol,name')
    codes = codes[codes['ts_code'].str.endswith('SZ') | codes['ts_code'].str.endswith('SH')]
    codes = codes.reset_index(drop=True)
    codes.columns = ['股票代码', '通达信代码', '股票名称']

    sectors = ak.stock_board_concept_name_em()
    sectors['成分股数量'] = 0
    sector_map = pd.DataFrame()
    for i in sectors.index.to_list():
        print(sectors['板块代码'][i], sectors['板块名称'][i])
        temp = ak.stock_board_concept_cons_em(symbol=sectors['板块名称'][i])
        temp = temp.rename(columns={'代码': '通达信代码'})
        temp = temp.merge(codes, how='left', left_on=['通达信代码'], right_on=['通达信代码'])
        temp['板块名称'] = sectors['板块名称'][i]
        temp['交易日期'] = datetime.datetime.now().strftime('%Y-%m-%d')
        temp['交易日期'] = pd.to_datetime(temp['交易日期'])
        temp['成分股数量'] = temp.shape[0]
        sectors.loc[sectors['板块名称'] == sectors['板块名称'][i], '成分股数量'] = temp.shape[0]
        temp = temp[['交易日期', '股票代码', '股票名称', '板块名称', '成分股数量', '最新价', '涨跌幅', '涨跌额', '成交量', '成交额',
                     '振幅', '最高', '最低', '今开', '昨收', '换手率', '市盈率-动态', '市净率']]
        temp.columns = ['交易日期', '股票代码', '股票名称', '板块名称', '成分股数量', '收盘价', '涨跌幅', '涨跌额', '成交量', '成交额',
                        '振幅', '最高价', '最低价', '开盘价', '昨日收盘价', '换手率', '市盈率-动态', '市净率']
        sector_map = pd.concat([sector_map, temp], axis=0)
        time.sleep(0.5)
    sector_map.to_csv('/root/ChatStock/data/data2_东方财富概念板块成分.csv', encoding='gbk', index=False)
    print(sector_map)

    sectors.to_csv('/root/ChatStock/data/data2_东方财富概念板块.csv', encoding='gbk', index=False)
    print(sectors)
