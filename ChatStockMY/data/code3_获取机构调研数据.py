# -*- coding: utf-8 -*-
import tushare as ts
import pandas as pd


def get_survey(code):
    ts.set_token('37d5b9baef929464347ddaa08240f1179ffe4af894faf7b14d8b3b5c')
    results = ts.pro_api().stk_surv(ts_code=code, fields='ts_code,name,surv_date,content')
    results.columns = ['股票代码', '股票名称', '公告日期', '机构调研内容']
    results['公告日期'] = pd.to_datetime(results['公告日期'], format='%Y%m%d'):wq
    return results


if __name__ == '__main__':
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    print(get_survey('002223.SZ'))


