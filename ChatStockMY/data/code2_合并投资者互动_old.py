# -*- coding: utf-8 -*-
import datetime
import pandas as pd
import glob



if __name__ == '__main__':
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    results = pd.read_csv('/root/ChatStock/data/data2_东方财富概念板块成分.csv', encoding='gbk')
    results = results[results['成分股数量'] <= 200]
    results = results[['股票代码', '股票名称', '板块名称']].dropna()
    results.columns = ['股票代码', '股票名称', '概念板块']
    results = results.groupby(by=['股票代码', '股票名称'])['概念板块'].apply(','.join).reset_index()
    sectors = results.copy()
    print(sectors)

    results = pd.read_csv('data2_投资者互动.csv', encoding='gbk')
    results['回答时间'] = pd.to_datetime(results['回答时间'])
    results['日期'] = results['回答时间'].dt.date
    results['投资者互动内容'] = results['问题'] + ' ' + results['回答']
    results = results[['日期', '回答时间', '股票代码', '股票名称', '投资者互动内容']]
    results = results.merge(sectors, how='inner', left_on=['股票代码', '股票名称'], right_on=['股票代码', '股票名称'])
    results = results.drop_duplicates()
    print(results)
    results.to_csv('/root/ChatStock/data/data2_投资者互动合并.csv', encoding='gbk', index=False, errors='ignore')

    # 2.互动抓取，同行业对比覆盖
    #
    #     	页面交互：
    #     		直接输出一个echart的柱状图，柱子是横着的，key是概念板块，value是概念板块中投资者互动的次数
    #     		取前5的概念板块并展示所有投资者互动的内容
    #     		取前5的概念板块并展示所有投资者互动的内容，GPT总结后输出
    #
    #     	python:
    #         	爬虫完成获取投资者互动，并存在csv中
    #
    #         prompt:
    #         	Now pretend you are expert making summary of long text.
    # 		Answer me with short summary in Chinese.
    # 		Summarize 股票代码, 股票名称, 日期, 投资者互动内容 of the following long text
    #
    # 		long text:
    # 			...