# -*- coding: utf-8 -*-

import time
import pandas as pd
import requests
import datetime
from feapder.network.selector import Selector
import json
from feapder.utils.log import log


columns = ['id', '股票代码', '股票名称', '问题时间', '问题', '回答时间', '回答']
n = 1


def stock_code_formatter(code):
    code = str(code)
    if code.startswith('60') or code.startswith('68'):
        code = code + '.SH'
    elif code.startswith('00') or code.startswith('30'):
        code = code + '.SZ'
    else:
        code = code + 'BJ'
    return code


def deal_datatime(time_str):
    if '小时' in time_str:
        hour = int(time_str.replace("小时前", ''))
        now = datetime.datetime.now()
        one_hour_ago = now - datetime.timedelta(hours=hour)
        return one_hour_ago
    elif '年' in time_str:
        date_string = time_str.replace("年", '-').replace("月", '-').replace("日", '')
        date_time = datetime.datetime.strptime(date_string, "%Y-%m-%d %H:%M")
        return date_time
    elif '昨天' in time_str:
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        date_string = time_str.replace('昨天', str(yesterday))
        date_time = datetime.datetime.strptime(date_string, "%Y-%m-%d %H:%M")
        return date_time


def get_sh(page):
    headers = {
        "Accept": "text/html, */*; q=0.01",
        "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "Referer": "http://sns.sseinfo.com/",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 Edg/113.0.1774.42",
        "X-Requested-With": "XMLHttpRequest"
    }
    url = "http://sns.sseinfo.com/ajax/feeds.do"
    params = {
        "type": "11",
        "pageSize": "10",
        "lastid": "-1",
        "show": "1",
        "page": page
    }
    response = requests.get(url, headers=headers, params=params, verify=False)
    selector = Selector(response.text)

    results = pd.DataFrame(columns=columns)
    items = selector.xpath('//div[@class="m_feed_item"]')
    for item in items:
        _id = item.xpath('./@id').extract_first()
        question_code = "".join(item.xpath('./div[@class="m_feed_detail m_qa_detail"]//div[@class="m_feed_txt"]/a/text()').extract()).strip()
        code = stock_code_formatter(question_code.replace(')', '').split('(')[1])
        name = question_code.replace(')', '').replace(':', '').split('(')[0]
        question_text = "".join(item.xpath('./div[@class="m_feed_detail m_qa_detail"]//div[@class="m_feed_txt"]/text()').extract()).strip()
        question_time = "".join(item.xpath('./div[@class="m_feed_detail m_qa_detail"]//div[@class="m_feed_from"]/span/text()').extract()).strip()
        question_time = deal_datatime(question_time)
        answer_text = "".join(item.xpath('./div[@class="m_feed_detail m_qa"]//div[@class="m_feed_cnt"]/div[@class="m_feed_txt"]/text()').extract()).strip()
        answer_time = "".join(item.xpath('./div[@class="m_feed_detail m_qa"]//div[@class="m_feed_from"]/span/text()').extract()).strip()
        answer_time = deal_datatime(answer_time)
        results.loc[results.shape[0]] = [_id, code, name, question_time, question_text, answer_time, answer_text]
    return results


def get_sz(page):

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    data = {'pageNo': str(page), 'pageSize': '10', 'searchTypes': '11,', 'market': '', 'industry': '', 'stockCode': ''}

    response = requests.post('http://irm.cninfo.com.cn/ircs/index/search',  headers=headers, data=data, verify=False)
    getJson = json.loads(response.text)

    results = pd.DataFrame(columns=columns)
    for cont in getJson['results']:
        _name = cont['companyShortName']
        _code = stock_code_formatter(cont['stockCode'])
        request_id = cont['indexId']
        requestion = cont['mainContent']
        requestion_time_stamp = int(cont['pubDate'])
        answear = cont['attachedContent']
        answaer_time_stamp = int(cont['attachedPubDate'])

        r = [request_id, _code, _name, requestion_time_stamp, requestion, answaer_time_stamp, answear]
        results.loc[results.shape[0]] = r
    results['问题时间'] = pd.to_datetime(results['问题时间'], unit='ms')
    results['回答时间'] = pd.to_datetime(results['回答时间'], unit='ms')
    return results


if __name__ == '__main__':
    # pd.set_option('max_columns', None)
    pd.set_option('display.unicode.ambiguous_as_wide', True)
    pd.set_option('display.unicode.east_asian_width', True)
    pd.set_option('display.width', 1000)

    page = 1
    results = pd.DataFrame()
    while True:
        temp = get_sh(page)
        results = pd.concat([results, temp], axis=0)
        page += 1
        time.sleep(0.5)
        print(temp['回答时间'].max(), datetime.datetime.now() - datetime.timedelta(days=n))
        if temp['回答时间'].max() < datetime.datetime.now() - datetime.timedelta(days=n):
            break

    page = 1
    while True:
        temp = get_sz(page)
        # print(temp)
        results = pd.concat([results, temp], axis=0)
        page += 1
        time.sleep(0.5)
        print(temp['回答时间'].max(), datetime.datetime.now() - datetime.timedelta(days=n))
        if temp['回答时间'].max() < datetime.datetime.now() - datetime.timedelta(days=n):
            break
    results = results[results['回答时间'] >= datetime.datetime.now() - datetime.timedelta(days=n)]
    results = results.sort_values(by=['回答时间'], ascending=False).reset_index(drop=True)
    results.to_csv('/root/ChatStock/data/data2_投资者互动.csv', encoding='gbk', index=False, errors='ignore')
    log.info(results)









