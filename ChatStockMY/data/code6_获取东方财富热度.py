# -*- coding: utf-8 -*-
import json
import time
import tushare as ts
import pandas as pd
import requests
import execjs
import os
from datetime import date
from datetime import datetime


def help(page):
    with open('/root/ChatStock/data/get_cookie.js', 'r', encoding='utf-8') as f:
        data = f.read()
    ctx = execjs.compile(data)
    headers = {
        "Accept": "*/*",
        "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Pragma": "no-cache",
        "Referer": "https://guba.eastmoney.com/",
        "Sec-Fetch-Dest": "script",
        "Sec-Fetch-Mode": "no-cors",
        "Sec-Fetch-Site": "cross-site",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 Edg/113.0.1774.42",
        "sec-ch-ua": "\"Microsoft Edge\";v=\"113\", \"Chromium\";v=\"113\", \"Not-A.Brand\";v=\"24\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\""
    }
    url = "https://gbcdn.dfcfw.com/rank/popularityList.js"
    params = {
        "type": "0",
        "sort": "0",
        "page": str(page),
        "v": "2023_5_18_15_32"
    }
    response = requests.get(url, headers=headers, params=params)
    results = pd.DataFrame(columns=['代码', '热度排名', '热度排名变化', '铁杆粉丝', '新晋粉丝'])
    for j in json.loads(ctx.call('window.d', response.text.replace('var popularityList=', '').replace('\'', ''))):
        r = [j.get('code'), j.get('rankNumber'), j.get('changeNumber'), j.get('ironsFans'), j.get('newFans')]
        results.loc[results.shape[0]] = r
    return results


def get_hotness():
    ts.set_token('37d5b9baef929464347ddaa08240f1179ffe4af894faf7b14d8b3b5c')
    stock_info = ts.pro_api().stock_basic(fields='ts_code,name,symbol')
    stock_info.columns = ['股票代码', '代码', '股票名称']
    print(stock_info)

    hotness = pd.DataFrame()
    for i in range(1, 6):
        hotness = pd.concat([hotness, help(i)], axis=0)
        time.sleep(0.5)
    hotness = stock_info.merge(hotness, how='inner', left_on=['代码'], right_on=['代码'])
    hotness = hotness.drop(['代码'], axis=1)
    
    source_file = '/root/ChatStock/data/data6_获取东方财富热度.py'  # 替换为源文件的路径

    if os.path.exists(source_file):
        file_name, file_extension = os.path.splitext(source_file)
        current_date = datetime.now().strftime("%Y%m%d_%H%M%S")
        destination_file = f"{file_name}_{current_date}{file_extension}"
        
        os.rename(source_file, destination_file)
        
    hotness.to_csv(source_file, encoding='utf-8', index=False, errors='ignore')
    return hotness


if __name__ == '__main__':
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    print(get_hotness())

