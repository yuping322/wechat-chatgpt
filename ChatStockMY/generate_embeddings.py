# !/usr/bin/env python3
"""
==== No Bugs in code, just some Random Unexpected FEATURES ====
┌─────────────────────────────────────────────────────────────┐
│┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐│
││Esc│!1 │@2 │#3 │$4 │%5 │^6 │&7 │*8 │(9 │)0 │_- │+= │|\ │`~ ││
│├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┤│
││ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{[ │}] │ BS  ││
│├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤│
││ Ctrl │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  ││
│├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────┬───┤│
││ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│Shift │Fn ││
│└─────┬──┴┬──┴──┬┴───┴───┴───┴───┴───┴──┬┴───┴┬──┴┬─────┴───┘│
│      │Fn │ Alt │         Space         │ Alt │Win│   HHKB   │
│      └───┴─────┴───────────────────────┴─────┴───┘          │
└─────────────────────────────────────────────────────────────┘
将文章（文本）计算Embedding并存放到本地。

Author: pankeyu
Date: 2023/05/05
"""
import os
import datetime
from typing import List

from tqdm import tqdm
from langchain.embeddings.huggingface import HuggingFaceEmbeddings
from langchain.vectorstores import FAISS
from langchain.document_loaders import UnstructuredFileLoader
from langchain.docstore.document import Document
from langchain.document_loaders import TextLoader
from langchain.indexes import VectorstoreIndexCreator

from utils.chinese_text_splitter import ChineseTextSplitter
from configs.config import EMBEDDING_MODEL, EMBEDDING_DEVICE
import os
os.environ["OPENAI_API_KEY"] = "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"
from langchain.llms import OpenAI


embeddings = HuggingFaceEmbeddings(
    model_name=EMBEDDING_MODEL,
    model_kwargs={'device': EMBEDDING_DEVICE}
)


def init_knowledge_vector_store(
        docs: List[str],
        embedding_saved_path: str = None
    ):
    """
    初始化知识库，将所有的文档都切分后存到本地。

    Args:
        docs (List[str]): 需要添加的句子。
        embedding_saved_path (str): Embedding 存放路径。
    """     
    if len(docs) > 0:
        if embedding_saved_path and os.path.isdir(embedding_saved_path):
            vector_store = FAISS.load_local(embedding_saved_path, embeddings)
            vector_store.add_documents(docs)
        else:
            if not embedding_saved_path:
                embedding_saved_path = f'doc_embeddings/FAISS_{datetime.datetime.now().strftime("%Y%m%d_%H%M%S")}'
            vector_store = FAISS.from_documents(docs, embeddings)

        vector_store.save_local(embedding_saved_path)
        print(f'Embedding has saved at: {embedding_saved_path}.')
        return embedding_saved_path


    
if __name__ == '__main__':
    # loader = TextLoader('ChatStock/data/user1.txt')
    # print('loader done.')
    # llm = OpenAI(model_name="gpt-4")
    # index = VectorstoreIndexCreator().from_loaders([loader])
    # print('index done.')
    # query = "这个用户看好哪些板块？"
    # index.query_with_sources(query)


    # docs = [
    #     "回复@少年徐永远乐观: AI在传媒、影视、游戏行业落地最快\n这段时间这几个板块的暴走有直接关系//@少年徐永远乐观:回复@轮回666:AIGC和CHATGPT是目前比较先进的人工智能技术，可以在以下方面帮助光线传媒优化研发成本：\n1. 剧本和故事创作：AIGC和CHATGPT可以用于辅助剧本和故事的创作。通过输入已有的故事情节和人物设定等信息，让AI模型生成新的故事情节和角色设定，减少人力的参与。这样不仅提高了效率，还可以降低研发成本。\n2. 视频剪辑和后期制作：AIGC可以用于视频剪辑和后期制作过程中。AI技术可以通过分析视频素材，自动提取出最佳的素材，进行剪辑和后期制作，减少人力的参与和时间的浪费。\n3. 视觉特效和CGI制作：AIGC和CHATGPT可以用于视觉特效和CGI制作过程中。AI技术可以通过模拟物理现象，生成更加逼真的视觉效果，如火山爆发、海浪拍岸等，从而减少人力的投入和时间的浪费。\n4. 游戏开发：AIGC和CHATGPT可以用于游戏开发中的各个环节。比如，通过训练AI模型，可以加速游戏场景的构建、角色的动画制作，还可以辅助游戏设计，提高游戏的可玩性和趣味性。\n综上所述，AIGC和CHATGPT可以在多个领域辅助光线传媒的研发工作，减少研发成本，提高效率。但是，AI技术还处于发展初期，不可避免地还会存在很多问题和局限性，光线传媒需要根据实际情况综合考虑使用AI技术。",
    #     "我觉得我选的很多票都很好\n即使我走后\n也没有走完\n汤姆猫、昆仑万维、剑桥科技、高澜股份、网宿科技、宝通科技、掌趣科技、光线传媒\n我已经出货的票里\n在我走后\n都出现了新高度\n而且往往高出一大截\n每次选票我都是投入了极大的精力\n反复推敲\n这样即使我走后也还会有资金来玩\n我只吃自己舒服的一段",
    #     "说句大话\n这些年以来\n我是眼看着中国产业一步步升级的\n老老年间\n我们8亿件衣服换一架空客飞机\n大量的人口密集型产业转移到我国大陆\n当年的出口老三样们：衣服、家电家具、玩具\n这些都属于人口密集型产业\n我们依靠着人口红利完成了对整个产业的席卷\n但是随着人口老龄化、生育率下降、人工成本上升等几大因素\n这些人口密集型产业发生了二次转移\n开始向我国西部和东南亚转移\n另一方面我们开始推动整个产业链的升级\n开始大力发展技术、知识密集型产业\n来看看出口新三样们：光伏、新能源车、电池、游戏及app\n光伏全产业链90%在我们这边\n全球最大的电池厂是宁德时代和比亚迪\n今年汽车出口将登顶全球\n在老美app下载排行榜前五名有四名是中国企业（拼多多、tiktok、剪映、希音）\n其实这两年以原神为代表的国产游戏也在海外有崛起之势\nchatGPT大幅降低游戏制作成本已经是共识\n很多人担心内卷\n其实游戏是最适合外卷的企业\n我们在基础软件比较薄弱\n但是在应用软件类还是很强大的\n卷到国外去\n一边赚钱一边输出文化"
    # ]
    # docsall=[]
    # i =0
    # for x in docs:
    #     d =Document(page_content=x, lookup_str='', metadata={'source': str(i)}, lookup_index=0)
    #     # d =Document()
    #     docsall.append(d)
    #     i=i+1
    # embedding_saved_path=init_knowledge_vector_store(
    #     docs=docsall
    # )
    # embedding_saved_path ="/workspace/wechat-chatgpt/doc_embeddings/FAISS_20230517_091635"
    # vector_store = FAISS.load_local(embedding_saved_path, embeddings)
    # query = "AI在传媒、影视、游戏行业落地最快"
    # docs = vector_store.similarity_search(query)
    # for d in docs:
    #     print(d.metadata["source"])
    i =0
    docsall=[]
    for line in open("/workspace/wechat-chatgpt/ChatStockMY/data/data4_投资者互动.csv"):  
        print(line[0:9]+"     "+str(i))
        d =Document(page_content=line, lookup_str='', metadata={'source': str(i)}, lookup_index=0)
        docsall.append(d)
        i=i+1

    embedding_saved_path=init_knowledge_vector_store(
    docs=docsall
    )
    print(embedding_saved_path)