# !/usr/bin/env python3
"""
==== No Bugs in code, just some Random Unexpected FEATURES ====
┌─────────────────────────────────────────────────────────────┐
│┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐│
││Esc│!1 │@2 │#3 │$4 │%5 │^6 │&7 │*8 │(9 │)0 │_- │+= │|\ │`~ ││
│├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┤│
││ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{[ │}] │ BS  ││
│├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤│
││ Ctrl │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  ││
│├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────┬───┤│
││ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│Shift │Fn ││
│└─────┬──┴┬──┴──┬┴───┴───┴───┴───┴───┴──┬┴───┴┬──┴┬─────┴───┘│
│      │Fn │ Alt │         Space         │ Alt │Win│   HHKB   │
│      └───┴─────┴───────────────────────┴─────┴───┘          │
└─────────────────────────────────────────────────────────────┘
请求 MySQL 数据库。

Author: pankeyu
Date: 2023/05/06
"""
import pymysql


class MySQLClient(object):

    db = pymysql.connect(
        host='49.51.184.126',
        user='root',
        password='W8wQw%^N5&ZDVf',
        database='stock_data'
    )

    def excute_sql(
            self, 
            sql: str
        ) -> tuple:
        """
        执行生成的sql语句。

        Args:
            sql (str): sql语句。

        Returns:
            tuple: _description_
        """
        try:
            cursor = self.db.cursor()
            cursor.execute(sql.replace(";", "")+" limit 3;")
            print(sql+" limit 3")
            rowList = cursor.fetchall()
            return rowList
        except:
            return ()



if __name__ == '__main__':
    client = MySQLClient()

    sql = """
    select 头像 from 公司管理层信息
    """
    res = client.excute_sql(sql)
    print(res)