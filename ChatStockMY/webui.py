# !/usr/bin/env python3
"""
==== No Bugs in code, just some Random Unexpected FEATURES ====
┌─────────────────────────────────────────────────────────────┐
│┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐│
││Esc│!1 │@2 │#3 │$4 │%5 │^6 │&7 │*8 │(9 │)0 │_- │+= │|\ │`~ ││
│├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┤│
││ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{[ │}] │ BS  ││
│├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤│
││ Ctrl │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  ││
│├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────┬───┤│
││ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│Shift │Fn ││
│└─────┬──┴┬──┴──┬┴───┴───┴───┴───┴───┴──┬┴───┴┬──┴┬─────┴───┘│
│      │Fn │ Alt │         Space         │ Alt │Win│   HHKB   │
│      └───┴─────┴───────────────────────┴─────┴───┘          │
└─────────────────────────────────────────────────────────────┘

WebUI。

Author: pankeyu
Date: 2023/05/04
"""
import time
import json
import datetime

import streamlit as st
from streamlit_chat import message

from agents import StockAgent
from config import STREAMING_LENGTH, MAX_HISTORY_NUM


LOG_PATH = './logs/web.log'


st.set_page_config(
    page_title="iStock", 
    page_icon=":robot_face:"
)


if 'user_query' not in st.session_state:
    st.session_state['user_query'] = ''


if 'stock_agent' not in st.session_state:
    st.session_state['stock_agent'] = StockAgent() 


if 'history' not in st.session_state:
    st.session_state['history'] = []



def store_history(current_message: dict):
    """
    将当前对话存到历史信息中。

    Args:
        current_message (dict): {
            'role': 'user',
            'content': 'xxxx'
        }
    """
    if len(st.session_state['history']) == MAX_HISTORY_NUM:
        st.session_state['history'].pop(0)
    st.session_state['history'].append(current_message)


def get_response(user_prompt: str):
    """
    用户的查询条件。

    Args:
        user_prompt (str): _description_
    """
    # return '贵州茅台的当前热度是1，贵州茅台是一个快递放假快乐圣诞节福利打开了手机翻到了快速反击打了。'
    return st.session_state['stock_agent'].run(user_prompt)


def show_response(
        answer_block,
        response: str,
        streaming: bool
    ):
    """
    显示查询结果。

    Args:
        answer_block: 用于展示答案的框。
        response (str): 模型返回结果。
        streaming (bool): 是否流式显示。
    """
    with st.expander('', expanded=True):
        if streaming:
            for i in range(0, len(response), STREAMING_LENGTH):
                answer_block.markdown(f'**:blue[📊 Stock Bot]**\n\n:green[{response[:i+STREAMING_LENGTH]}]')
                time.sleep(0.01)
        else:
            answer_block.markdown(f'**:blue[📈 Stock Bot]**')
            answer_block.markdown(response)


def show_history_messages(
        history_container,
    ):
    """
    显示历史对话记录。

    Args:
        history_block (_type_): _description_
    """
    with history_container:
        with st.expander('', expanded=True):
            for i, history in enumerate(st.session_state['history']):
                if history['role'] == 'user':
                    message(history['content'], is_user=True, key=str(i) + '_user')
                else:
                    message(history['content'], is_user=False, key=str(i))

def main():
    """
    main func.
    """
    st.markdown("<h1 style='text-align: center;'>iStock</h1>", unsafe_allow_html=True)

    query = st.text_input(
        '搜索框',
        max_chars=500,
        label_visibility='hidden',
        placeholder='贵州茅台现在的热度排名...'
    )
    answer_block = st.empty()                               # 当前模型返回结果显示区域
    history_container = st.container()                      # 对话历史信息显示区域
        
    if query and query != st.session_state['user_query']:
        st.session_state['user_query'] = query
        store_history({
            'role': 'user',
            'content': query
        })

        with st.spinner('我正在思考...'):
            response = get_response(query)

        with open(LOG_PATH, 'a', encoding='utf8') as f:
            current_sample = {
                'time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'query': query,
                'response': response
            }
            f.write(f'{json.dumps(current_sample, ensure_ascii=False)}\n')
        
        show_response(
            answer_block,
            response, 
            streaming=True
        )

        store_history({
            'role': 'assistant',
            'content': response
        })

        show_history_messages(history_container)


if __name__ == '__main__':
    main()
