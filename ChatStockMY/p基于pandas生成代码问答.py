"""Example of using PandasAI with a Pandas DataFrame"""

import pandas as pd
from pandasai import PandasAI
from pandasai.llm.openai import OpenAI
import os
os.environ["OPENAI_API_KEY"] = "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"
openai_api_key='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'


import streamlit as st
from streamlit_chat import message

llm = OpenAI(model_name="gpt-4",temperature=0, openai_api_key=openai_api_key)



def get_stock_info():
    dataframe = {
        "country": [
            "United States",
            "United Kingdom",
            "France",
            "Germany",
            "Italy",
            "Spain",
            "Canada",
            "Australia",
            "Japan",
            "China",
        ],
        "gdp": [
            21400000,
            2940000,
            2830000,
            3870000,
            2160000,
            1350000,
            1780000,
            1320000,
            516000,
            14000000,
        ],
        "happiness_index": [7.3, 7.2, 6.5, 7.0, 6.0, 6.3, 7.3, 7.3, 5.9, 5.0],
    }


    df = pd.DataFrame(dataframe)
    return df 


@st.cache_data
def convert_to_csv(df):
    # IMPORTANT: Cache the conversion to prevent computation on every rerun
    return df.to_csv(index=False).encode('utf-8')

def main():
    all_stock_info = get_stock_info()
    st.set_page_config(page_title="pandas 智能问答")
    st.header("pandas 智能问答 💬")
    
    st.write(all_stock_info)
    
    
    csv = convert_to_csv(all_stock_info)
    # download button 1 to download dataframe as csv
    download1 = st.download_button(
        label="Download data as CSV",
        data=csv,
        file_name='large_df.csv',
        mime='text/csv'
    )

    st.markdown("<h1 style='text-align: center;'>输入一个基于以上表格的问题</h1>", unsafe_allow_html=True)

    query = st.text_input(
        '问题',
        max_chars=500,
        # label_visibility='hidden',
        value='Calculate the sum of the gdp of north american countries',
        placeholder='Calculate the sum of the gdp of north american countries'
    )
    answer_block = st.empty()   
    if query :
        pandas_ai = PandasAI(llm, verbose=True, conversational=False)
        (response,code) = pandas_ai.run(all_stock_info, query,show_code=True)
        answer_block.markdown(response)
        st.code(code,language='python',line_numbers=True)



if __name__ == '__main__':
    main()
            
        



