# !/usr/bin/env python3
"""
==== No Bugs in code, just some Random Unexpected FEATURES ====
┌─────────────────────────────────────────────────────────────┐
│┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐│
││Esc│!1 │@2 │#3 │$4 │%5 │^6 │&7 │*8 │(9 │)0 │_- │+= │|\ │`~ ││
│├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┤│
││ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{[ │}] │ BS  ││
│├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤│
││ Ctrl │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  ││
│├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────┬───┤│
││ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│Shift │Fn ││
│└─────┬──┴┬──┴──┬┴───┴───┴───┴───┴───┴──┬┴───┴┬──┴┬─────┴───┘│
│      │Fn │ Alt │         Space         │ Alt │Win│   HHKB   │
│      └───┴─────┴───────────────────────┴─────┴───┘          │
└─────────────────────────────────────────────────────────────┘

测试 LangChain。

Author: pankeyu
Date: 2023/05/01
"""
import os
os.environ["OPENAI_API_KEY"] = "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"

from rich import print

from langchain.llms import OpenAI
from langchain.agents import initialize_agent, AgentType

from custom_tools import *


class StockAgent(object):
    """
    Basic Stock Agent.

    Args:
        object (_type_): _description_
    """

    def __init__(self) -> None:
        """
        init func.
        """
        self.llm = OpenAI(temperature=0)
        self.agent = initialize_agent(
            [
                tool_of_select_information_from_database,
                tool_of_find_stock_rank,
                tool_of_find_account_message,
                tool_of_find_macro_population
            ],
            self.llm,
            agent=AgentType.ZERO_SHOT_REACT_DESCRIPTION,
            verbose=True
        )

    def run(
            self, 
            user_prompt: str
        ) -> str:
        """
        call func.

        Arguments:
            user_prompt (str): 用户的搜索query。

        Returns:
            Any: _description_
        """
        res = self.agent.run(user_prompt)
        return res
    

if __name__ == '__main__':
    sa = StockAgent()
    res = sa.run(
        # "查询2023-05-09，投资者互动表中的概念板块列"
        # "轮回666这个用户对平安银行的看法"
        # "甚嘛扯淡股对新华网的看法"
        # "雪球上的轮回666今天说了啥"
        # "贵州茅台的热度排行"
        # "sst金狐狸推荐股票"
        # "贵州茅台的管理层信息"
        # "刘世仲是贵州茅台的管理层吗?"
        # "帮我总结下平安银行的公司管理层信息"
        # "平安银行的看法"
        # "查询2023-05-09，投资者互动表中的概念板块列前三条"
        # "查询剑桥科技的热度"
        # "查询主力净流入大的概念板块有哪些"
        "查询中国总人口数据"
    )
    print('\n\nFinal Results: \n', res)