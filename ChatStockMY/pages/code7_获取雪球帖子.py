import json
import time
import tushare as ts
import pandas as pd
import requests
import execjs
import os
import streamlit as st



if __name__ == '__main__':
    pd.set_option('display.max_rows', 1000)
    pd.set_option('display.max_columns', 1000)
    pd.set_option('display.width', 7000)

    results = pd.read_csv('/root/ChatStock/data/data7_雪球帖子.csv', encoding='utf-8')
    st.dataframe(results, width=7000, height=7000)
    print(results)