from dotenv import load_dotenv
import streamlit as st
from PyPDF2 import PdfReader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.llms import OpenAI
from langchain.callbacks import get_openai_callback

import os
os.environ["OPENAI_API_KEY"] = "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"

import datetime
import pandas as pd
import glob

openai_api_key='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'
from langchain.chat_models import ChatOpenAI
from langchain.schema import HumanMessage, SystemMessage, AIMessage
from langchain import PromptTemplate
import numpy as np


# llm = ChatOpenAI(model_name="gpt-4", temperature=0, openai_api_key=openai_api_key)
llm = OpenAI(model_name="gpt-4",temperature=0, openai_api_key=openai_api_key)



def getSum(content):
  template = """
  Now pretend you are expert making summary of long text. 
                                Answer me with  summary in Chinese.
                                Summarize 姓名, 个人简历 of the following long text
  
  
  """
    
  summary_prompt = template +str(content)
  summary = llm(summary_prompt)
  return summary.strip()

def getQuestion(query,content):
    template = """
    Answer the question based on the long text. Each line of the long text contains 股票名称 and 投资者互动 separated by a blank space.
    Answer me in short form in Chinese. Please always provide answer with 股票名称.
    Question: 
    
    """

    summary_prompt = template +str(query)+"/n/nLong Text:/n/n "+str(content)
    summary = llm(summary_prompt)
    
    return summary.strip()

def starts_with_digit(string):
    if string and (string[0].isdigit() or string[0].isnumeric()):
        return True
    return False
  

def sectors_results():
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    results = pd.read_csv('/root/ChatStock/data/data2_东方财富概念板块成分.csv', encoding='gbk')
    results = results[results['成分股数量'] <= 200]
    results = results[['股票代码', '股票名称', '板块名称']].dropna()
    results.columns = ['股票代码', '股票名称', '概念板块']
    results = results.groupby(by=['股票代码', '股票名称'])['概念板块'].apply(','.join).reset_index()
    sectors = results.copy()
    print(sectors)

    results = pd.read_csv('/root/ChatStock/data/data2_投资者互动.csv', encoding='gbk')
    results['回答时间'] = pd.to_datetime(results['回答时间'])
    results['日期'] = results['回答时间'].dt.date
    results['投资者互动内容'] = results['问题'] + ' ' + results['回答']
    results = results[['日期', '回答时间', '股票代码', '股票名称', '投资者互动内容']]
    results = results.merge(sectors, how='inner', left_on=['股票代码', '股票名称'], right_on=['股票代码', '股票名称'])
    results = results.drop_duplicates()
    print(results)
    return results  


def main():
    load_dotenv()
    st.set_page_config(page_title="互动抓取，同行业对比覆盖")
    st.header("互动抓取，同行业对比覆盖 💬")
    
    st.markdown("<h1 style='text-align: center;'>板块统计</h1>", unsafe_allow_html=True)
    results = pd.read_csv('/root/ChatStock/data/data2_投资者互动按概念板块统计.csv', encoding='gbk')

    chart_data = pd.Series(results['概念板块出现数'].values, index=results['概念板块'].values)

    # 绘制横向柱状图
    # st.bar_chart(chart_data)
    st.bar_chart(chart_data)
    
    # chart_data = pd.DataFrame(
    # np.random.randn(20, 3),
    # columns=['a', 'b', 'c'])

    # st.area_chart(chart_data)

    # st.markdown("<h1 style='text-align: center;'>输入公司名或者股票代码，只支持输入一个做总结</h1>", unsafe_allow_html=True)

    answer_block = st.empty() 
    
    stk=sectors_results()
    query = st.text_input(
        '投资者互动',
        max_chars=500,
        label_visibility='hidden',
        placeholder='投资者互动中哪个股票与国内宠物经济有关'
    )
    sum_stk=getQuestion(query,stk)
    
    
   
    st.dataframe(stk, width=7000, height=7000)


    answer_block.markdown(sum_stk)
    print(sum_stk)
    



if __name__ == '__main__':
    main()
