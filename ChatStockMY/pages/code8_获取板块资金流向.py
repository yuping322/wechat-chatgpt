# -*- coding: utf-8 -*-
import tushare as ts
import pandas as pd
import akshare as ak
from sqlalchemy import create_engine
import streamlit as st


if __name__ == '__main__':
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 7000)

    # results = ak.stock_sector_fund_flow_rank(indicator="今日", sector_type="概念资金流")
    # results = results[['名称', '今日主力净流入-净额', '今日主力净流入最大股']]
    # results.columns = ['概念板块', '主力净流入', '主力净流入最大股票']
    
    results = pd.read_csv('/root/ChatStock/data/data8_获取板块资金流向.csv', encoding='utf-8')
    st.dataframe(results, width=7000, height=7000)
    print(results)
    