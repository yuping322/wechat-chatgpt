import json
import time
import tushare as ts
import pandas as pd
import requests
import execjs
import os
import streamlit as st



if __name__ == '__main__':
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 7000)

    results = pd.read_csv('/root/ChatStock/data/data6_获取东方财富热度.csv', encoding='utf-8')
    st.dataframe(results, width=7000, height=7000)
    
    print(results)