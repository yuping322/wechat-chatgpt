from dotenv import load_dotenv
import streamlit as st
from PyPDF2 import PdfReader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.llms import OpenAI
from langchain.callbacks import get_openai_callback

import os
os.environ["OPENAI_API_KEY"] = "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"


from data.code1_公司管理层履历 import get_stock_info,get_stk_managers
openai_api_key='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'
from langchain.chat_models import ChatOpenAI
from langchain.schema import HumanMessage, SystemMessage, AIMessage
from langchain import PromptTemplate


# llm = ChatOpenAI(model_name="gpt-4", temperature=0, openai_api_key=openai_api_key)
llm = OpenAI(model_name="gpt-4",temperature=0, openai_api_key=openai_api_key)


def getSum(content):
  template = """
  Now pretend you are expert making summary of long text. 
                                Answer me with  summary in Chinese.
                                Summarize 姓名, 个人简历 of the following long text
  
  
  """

  summary_prompt = template +str(content)
  summary = llm(summary_prompt)
  return summary.strip()


def starts_with_digit(string):
    if string and (string[0].isdigit() or string[0].isnumeric()):
        return True
    return False
  
  
def main():
    all_stock_info = get_stock_info()
    load_dotenv()
    st.set_page_config(page_title="公司管理层履历")
    st.header("公司管理层履历 💬")
    
    st.markdown("<h1 style='text-align: center;'>输入公司名或者股票代码，只支持输入一个做总结</h1>", unsafe_allow_html=True)

    query = st.text_input(
        '公司名',
        max_chars=500,
        label_visibility='hidden',
        placeholder='贵州茅台'
    )
    answer_block = st.empty()   
    # query ="贵州茅台"
    if query :
        try:
            if not starts_with_digit(query):
                query=all_stock_info.loc[all_stock_info['股票名称'] == query, '股票代码'].values[0]
            print(str(query))
            stk=get_stk_managers(str(query))
            st.dataframe(stk, width=7000, height=7000)
            sum_stk =getSum(stk)
            answer_block.markdown(sum_stk)
            print(sum_stk)
        except:
            st.write("没有这支股票")

if __name__ == '__main__':
    main()
