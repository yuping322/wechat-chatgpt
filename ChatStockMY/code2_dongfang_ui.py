from dotenv import load_dotenv
import streamlit as st
from PyPDF2 import PdfReader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.llms import OpenAI
from langchain.callbacks import get_openai_callback

import os
os.environ["OPENAI_API_KEY"] = "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"

import datetime
import pandas as pd
import glob
import pandas as pd
import akshare as ak
import tushare as ts
import time
import datetime


openai_api_key='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'
from langchain.chat_models import ChatOpenAI
from langchain.schema import HumanMessage, SystemMessage, AIMessage
from langchain import PromptTemplate


# llm = ChatOpenAI(model_name="gpt-4", temperature=0, openai_api_key=openai_api_key)
llm = OpenAI(model_name="gpt-4",temperature=0, openai_api_key=openai_api_key)



def getSum(content):
  template = """
  Now pretend you are expert making summary of long text. 
                                Answer me with  summary in Chinese.
                                Summarize 姓名, 个人简历 of the following long text
  
  
  """

  summary_prompt = template +str(content)
  summary = llm(summary_prompt)
  return summary.strip()


def starts_with_digit(string):
    if string and (string[0].isdigit() or string[0].isnumeric()):
        return True
    return False
  

def sectors_results():
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    ts.set_token('37d5b9baef929464347ddaa08240f1179ffe4af894faf7b14d8b3b5c')
    codes = ts.pro_api().query('stock_basic', exchange='', list_status='L', fields='ts_code,symbol,name')
    codes = codes[codes['ts_code'].str.endswith('SZ') | codes['ts_code'].str.endswith('SH')]
    codes = codes.reset_index(drop=True)
    codes.columns = ['股票代码', '通达信代码', '股票名称']

    sectors = ak.stock_board_concept_name_em()
    sectors['成分股数量'] = 0
    sector_map = pd.DataFrame()
    for i in sectors.index.to_list():
        print(sectors['板块代码'][i], sectors['板块名称'][i])
        temp = ak.stock_board_concept_cons_em(symbol=sectors['板块名称'][i])
        temp = temp.rename(columns={'代码': '通达信代码'})
        temp = temp.merge(codes, how='left', left_on=['通达信代码'], right_on=['通达信代码'])
        temp['板块名称'] = sectors['板块名称'][i]
        temp['交易日期'] = datetime.datetime.now().strftime('%Y-%m-%d')
        temp['交易日期'] = pd.to_datetime(temp['交易日期'])
        temp['成分股数量'] = temp.shape[0]
        sectors.loc[sectors['板块名称'] == sectors['板块名称'][i], '成分股数量'] = temp.shape[0]
        temp = temp[['交易日期', '股票代码', '股票名称', '板块名称', '成分股数量', '最新价', '涨跌幅', '涨跌额', '成交量', '成交额',
                     '振幅', '最高', '最低', '今开', '昨收', '换手率', '市盈率-动态', '市净率']]
        temp.columns = ['交易日期', '股票代码', '股票名称', '板块名称', '成分股数量', '收盘价', '涨跌幅', '涨跌额', '成交量', '成交额',
                        '振幅', '最高价', '最低价', '开盘价', '昨日收盘价', '换手率', '市盈率-动态', '市净率']
        sector_map = pd.concat([sector_map, temp], axis=0)
        time.sleep(0.5)
    sector_map.to_csv('data2_东方财富概念板块成分.csv', encoding='utf-8', index=False)
    print(sector_map)

    sectors.to_csv('data2_东方财富概念板块.csv', encoding='utf-8', index=False)
    print(sectors)
    return results  


def main():
    stk=sectors_results()
    load_dotenv()
    st.set_page_config(page_title="东方财富概念板块，同行业对比覆盖")
    st.header("东方财富概念板块，同行业对比覆盖 💬")
    
    st.markdown("<h1 style='text-align: center;'>输入公司名或者股票代码，只支持输入一个做总结</h1>", unsafe_allow_html=True)

    answer_block = st.empty() 
    

    st.write(stk)
    sum_stk =getSum(stk)
    answer_block.markdown(sum_stk)
    print(sum_stk)
    

if __name__ == '__main__':
    main()
