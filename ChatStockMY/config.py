# !/usr/bin/env python3
"""
==== No Bugs in code, just some Random Unexpected FEATURES ====
┌─────────────────────────────────────────────────────────────┐
│┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐│
││Esc│!1 │@2 │#3 │$4 │%5 │^6 │&7 │*8 │(9 │)0 │_- │+= │|\ │~ ││
│├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┤│
││ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{[ │}] │ BS  ││
│├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤│
││ Ctrl │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  ││
│├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────┬───┤│
││ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│Shift │Fn ││
│└─────┬──┴┬──┴──┬┴───┴───┴───┴───┴───┴──┬┴───┴┬──┴┬─────┴───┘│
│      │Fn │ Alt │         Space         │ Alt │Win│   HHKB   │
│      └───┴─────┴───────────────────────┴─────┴───┘          │
└─────────────────────────────────────────────────────────────┘

Config File.

Author: pankeyu
Date: 2023/05/04
"""
from langchain import PromptTemplate


# Web UI Relavent
STREAMING_LENGTH = 1
MAX_HISTORY_NUM = 20


# Tools Relavent
MAX_ACCOUNT_MESSAGE_LEN = 2048           # 查找用户言论的最大长度

FIND_DB_TABLE_PROMPT_TEMPLATE = PromptTemplate(
    input_variables=['table_num', 'table_description', 'question'],
    template="""our database has {table_num} tables, the table structures are listed as follow:

{table_description}

according to user question, please tell me which table I can find the answer
only answer me with one word of the table name. do not tell me the reason
user question: {question}"""
)

FIND_INFO_PROMPT_PATTERN = PromptTemplate(
    input_variables=['question', 'table_description'],
    template="""Now pretend you are a mysql expert to translate english to mysql queries. You are given stucture of some tables and data examples in the previously mentioned tables. 
Generate a mysql query: {question}

stucture of tables:
{table_description}

Answer me with a single mysql, no extra messages."""
)


# Database Relavent
DATABASE_STRUCTURE = {
    '公司管理层信息': """- index: id of the row
- 股票代码: the unique code of a stock
- 股票名称: the name of a stock
- 公告日期: the date of 公司管理层信息 is announced
- 姓名: the last name and first name of 公司管理层 which is a person
- 性别: the gender of 公司管理层 which is a person
- 岗位类别: the position in board of 公司管理层 which is a person
- 岗位: the position in company of 公司管理层 which is a person
- 学历: the education of 公司管理层 which is a person
- 国籍: the nationality of 公司管理层 which is a person
- 个人简历: the resume of 公司管理层 which is a person""",
    '投资者互动表': """- index: id of the row
- 日期: the announcement date of 投资者互动 which is questions and answers between 投资者 and 董秘
- 股票代码: the unique code of a stock
- 股票名称: the name of a stock
- 投资者互动内容: questions and answers between 投资者 and 董秘
- 概念板块: 板块 or 概念板块 is a group of stocks shares something in common""",
    '概念板块与股票的对应关系表': """- index: id of the row
- 股票代码: the unique code of a stock
- 股票名称: the name of a stock
- 概念板块: 板块 or 概念板块 is a group of stocks shares something in common""",
    '概念板块资金流向': """- index: id of the row
- 概念板块: 板块 or 概念板块 is a group of stocks shares something in common
- 主力净流入: 大单买入成交额 - 大单卖出成交额
- 主力净流入最大股票: the name of stock with gets most attention in 概念板块""",
    '股票基本信息': """- index: id of the row
- 股票代码: the unique code of a stock
- 股票名称: the name of a stock
- 所属行业: the industry with the stock is belonging to
- 拼音缩写: 拼音 initials which is a type unique index of the stock
- 是否沪深港通标的: 沪港通 means the stock can be traded from hongkong by shanghai stock exchange. 深港通 means the stock can be traded from hongkong by shenzhen stock exchange. empty string mean the stock cannot be traded from hongkong""",
    '股票热度排名表': """- index: id of the row
- 股票代码: the unique code of a stock
- 股票名称: the name of a stock
- 热度: the number of user who has browsed the stock today. it indicates how hot the stock is.
- 热度变化量: change of the 热度 from yesterday to today
- 涨幅: change of price of the stock""",
    '雪球': """- index: id of the row
- `index`: id of the row
- `用户名`: the unique name of a user in 雪球 forum
- `发帖时间`: the time when 用户名 make a post
- `点赞数`: the number of other users who like the post
- `收藏数`: the number of other users who archive the post
- `回复数`: the number of other users who reply the post
- `转发数`: the number of other users who re-post the post
- `发帖内容`: the text content of the post. the content may or may not mention few stocks by their names"""
}