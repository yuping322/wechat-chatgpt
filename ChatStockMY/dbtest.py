import pprint
import pymysql
 
# 打开数据库连接
db = pymysql.connect(
    host='localhost',
    user='root',
    password='W8wQw%^N5&ZDVf',
    database='stock_data'
)

# 忽略掉系统库
IgnoreDB = {'information_schema', 'mysql', 'performance_schema', 'sys'}    
 
# 处理一个数据库
def ProcOneDB(dbName):
    print('************ use %s ************' % dbName)
    connDB = pymysql.connect(
        host='localhost',
        user='root',
        password='W8wQw%^N5&ZDVf',
        database='stock_data'
    )
    cur = connDB.cursor()
     
    sql = 'show tables;'
    cur.execute(sql)
    rowList = cur.fetchall()
    tableList = list()
    for row in rowList:
        tableList.append(row[0])
    print('tableList(%d):\n%s\n' % (len(tableList), pprint.pformat(tableList, indent=4)))
     
    # 处理每个表
    for tabName in tableList:
        print('table %s ...' % tabName)
        sql = "select column_name from information_schema.columns where table_schema='%s' and table_name='%s';"
        sql = sql % (dbName, tabName)
        cur.execute(sql)
        rowList = cur.fetchall()
        fieldList = list()
        for row in rowList:
            fieldList.append(row[0])
        print('fieldList(%d):\n%s\n' % (len(fieldList), pprint.pformat(fieldList, indent=4)))
     
    cur.close()
    connDB.close() 
 
# 处理所有数据库
def ProcAllDB():
    connDB = pymysql.connect(
        host='localhost',
        user='root',
        password='W8wQw%^N5&ZDVf',
        database='stock_data'
    )
                                 
    cur = connDB.cursor()
         
    sql = "show databases;" 
    print('input sql:' + sql)
    cur.execute(sql)
    rowList = cur.fetchall()
    cur.close()
    connDB.close()
     
    dbList = list()
    for row in rowList:
        dbList.append(row[0])
    print('dbList(%d):\n%s\n' % (len(dbList), pprint.pformat(dbList, indent=4)))
     
    for dbName in dbList:
        if dbName in IgnoreDB:
            continue
        ProcOneDB(dbName)                


def test_sql():
    connDB = pymysql.connect(
        host='localhost',
        user='root',
        password='W8wQw%^N5&ZDVf',
        database='stock_data'
    )
                                 
    cur = connDB.cursor()
    sql = """SELECT 姓名, 个人简历
    FROM 公司管理层信息
    WHERE 股票代码 = '601988.SH'"""

    cur.execute(sql)
    rowList = cur.fetchall()
    print(rowList)


if __name__ == '__main__':
    ProcAllDB()
    #test_sql()
