import re
from typing import List
from langchain.text_splitter import CharacterTextSplitter


class ChineseTextSplitter(CharacterTextSplitter):
    """
    适用于中文的文本分割，按照标点符号进行切割。

    Args:
        CharacterTextSplitter (_type_): _description_
    """
    
    def __init__(
            self, 
            pdf: bool = False, 
            **kwargs
        ):
        """
        init func.

        Args:
            pdf (bool, optional): _description_. Defaults to False.
        """
        super().__init__(**kwargs)
        self.pdf = pdf

    def split_text(
            self, 
            text: str
        ) -> List[str]:
        """
        文本分割。

        Args:
            text (str): _description_

        Returns:
            List[str]: _description_
        """
        if self.pdf:
            text = re.sub(r"\n{3,}", "\n", text)
            text = re.sub('\s', ' ', text)
            text = text.replace("\n\n", "")
        sent_sep_pattern = re.compile('([﹒﹔﹖﹗．。！？]["’”」』]{0,2}|(?=["‘“「『]{1,2}|$))')
        sent_list = []
        for ele in sent_sep_pattern.split(text):
            if sent_sep_pattern.match(ele) and sent_list:
                sent_list[-1] += ele
            elif ele:
                sent_list.append(ele)
        return sent_list


if __name__ == '__main__':
    splitter = ChineseTextSplitter()
    res = splitter.split_text("""
    刘德华（Andy Lau），1961年9月27日出生于香港新界大埔镇泰亨村[251]，毕业于第十期无线艺员训练班，中国香港男演员、歌手、中国残疾人福利基金会副理事长、制片人、作词人。
    1981年出演电影处女作《彩云曲》。1983年主演的武侠剧《神雕侠侣》在香港取得62点的收视纪录[1]。1985年发行首张个人专辑《只知道此刻爱你》[7]。1990年凭借专辑《可不可以》在歌坛获得关注。1991年创办天幕电影公司[2]。1994年获得十大劲歌金曲最受欢迎男歌星奖；同年担任剧情片《天与地》的制片人。1995年在央视春晚上演唱其代表作《忘情水》[8]。2000年凭借警匪片《暗战》获得金像奖最佳男主角奖[3]；同年被《吉尼斯世界纪录大全》评为获奖最多的香港男歌手[9]。
    """)
    print(res)