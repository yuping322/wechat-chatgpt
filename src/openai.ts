import {
  Configuration,
  CreateImageRequestResponseFormatEnum,
  CreateImageRequestSizeEnum,
  OpenAIApi
} from "openai";
import fs from "fs";
import DBUtils from "./data.js";
import {config} from "./config.js";
import axios from 'axios';


const configuration = new Configuration({
  apiKey: config.openai_api_key,
  basePath: config.api,
});
const openai = new OpenAIApi(configuration);

/**
 * Get completion from OpenAI
 * @param username
 * @param message
 */
async function chatgpt(username:string,message: string): Promise<string> {
  // 先将用户输入的消息添加到数据库中
  DBUtils.addUserMessage(username, message);
  const messages = DBUtils.getChatMessage(username);

  const response = await openai.createChatCompletion({
    model: "gpt-3.5-turbo",
    messages: messages,
    temperature: config.temperature,
  });
  let assistantMessage = "";
  try {
    if (response.status === 200) {
      assistantMessage = response.data.choices[0].message?.content.replace(/^\n+|\n+$/g, "") as string;
    }else{
      console.log(`Something went wrong,Code: ${response.status}, ${response.statusText}`)
    }
  }catch (e:any) {
    if (e.request){
      console.log("请求出错");
    }
  }
  return assistantMessage;
}

/**
 * Get completion from OpenAI
 * @param username
 * @param message
 */
async function chatcluade(username:string,message: string): Promise<string> {
  // 先将用户输入的消息添加到数据库中
  DBUtils.addUserMessage(username, message);
  const messages = DBUtils.getChatMessage(username);

  let assistantMessage = "";

  const data = {
    "prompt": message
  };
  await axios.post('http://127.0.0.1:8088/claude/chat', data, {
    headers: {
      'Content-Type': 'application/json',
      'accept': 'application/json',
      'x-token': '13832bf5-ed7c-4b5c-a30b-e7db48d25b86'
    }
  })
    .then((response) => {
      console.log("----------------");
      console.log(response.data);
      assistantMessage=response.data.claude as string;
      assistantMessage=assistantMessage.replace(/^\n+|\n+$/g, "");
      console.log("----------------");
    })
    .catch((error) => {
      console.log(error);
    });
 
    return assistantMessage;

  // const response = await openai.createChatCompletion({
  //   model: "gpt-3.5-turbo",
  //   messages: messages,
  //   temperature: config.temperature,
  // });
  // let assistantMessage = "";
  // try {
  //   if (response.status === 200) {
  //     assistantMessage = response.data.choices[0].message?.content.replace(/^\n+|\n+$/g, "") as string;
  //   }else{
  //     console.log(`Something went wrong,Code: ${response.status}, ${response.statusText}`)
  //   }
  // }catch (e:any) {
  //   if (e.request){
  //     console.log("请求出错");
  //   }
  // }
  // return assistantMessage;
}


/**
 * Get completion from OpenAI
 * @param username
 * @param message
 */
async function chatagent(username:string,message: string): Promise<string> {
  // 先将用户输入的消息添加到数据库中
  DBUtils.addUserMessage(username, message);
  const messages = DBUtils.getChatMessage(username);

  let assistantMessage = "";

  const data = {
    "prompt": message
  };
  await axios.post('http://127.0.0.1:8088/claude/agent', data, {
    headers: {
      'Content-Type': 'application/json',
      'accept': 'application/json',
      'x-token': '13832bf5-ed7c-4b5c-a30b-e7db48d25b86'
    }
  })
    .then((response) => {
      console.log("----------------");
      console.log(response.data);
      assistantMessage=response.data.claude as string;
      assistantMessage=assistantMessage.replace(/^\n+|\n+$/g, "");
      console.log("----------------");
    })
    .catch((error) => {
      console.log(error);
    });
 
    return assistantMessage;
}




/**
 * Get image from Dall·E
 * @param username
 * @param prompt
 */
async function dalle(username:string,prompt: string) {
  const response = await openai.createImage({
    prompt: prompt,
    n:1,
    size: CreateImageRequestSizeEnum._256x256,
    response_format: CreateImageRequestResponseFormatEnum.Url,
    user: username
  }).then((res) => res.data).catch((err) => console.log(err));
  if (response) {
    return response.data[0].url;
  }else{
    return "Generate image failed"
  }
}

/**
 * Speech to text
 * @param username
 * @param videoPath
 */
async function whisper(username:string,videoPath: string): Promise<string> {
  const file:any= fs.createReadStream(videoPath);
  const response = await openai.createTranscription(file,"whisper-1")
    .then((res) => res.data).catch((err) => console.log(err));
  if (response) {
    return response.text;
  }else{
    return "Speech to text failed"
  }
}

export {chatgpt,dalle,whisper,chatagent,chatcluade};