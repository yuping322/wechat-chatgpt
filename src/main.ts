import { WechatyBuilder } from "wechaty";
import QRCode from "qrcode";
import { ChatGPTBot } from "./bot.js";
import {config} from "./config.js";

import axios from 'axios';

const chatGPTBot = new ChatGPTBot();

const bot =  WechatyBuilder.build({
  name: "wechat-assistant", // generate xxxx.memory-card.json and save login data for the next login
  puppet: "wechaty-puppet-wechat",
  puppetOptions: {
    uos: true
  }
});
async function main() {

  // let url = 'https://img-home.csdnimg.cn/data_json/toolbar/toolbar1105.json';
  // let GetJsonData=function (url :string){
  //     // const https = require('https');
  //     https.get(url, (response:any) => {
  //         let data = '';
  //         //数据正在接收中...
  //         response.on('data', (chunk :String) => {
  //             data += chunk;
  //         });
  //         //数据接收完成
  //         response.on('end', () => {
  //             console.log('同步请求数据完成:',JSON.parse(data));
  //         });
   
  //     }).on("error", (error :any) => {
  //         console.log("Error: " + error.message);
  //     });
  // }
  // GetJsonData(url);
  // let assistantMessage222: string = 'ddddd';
  // assistantMessage222 = assistantMessage222.replace("World", "TypeScript");
  // console.log(assistantMessage222);

  // let assistantMessage = "";

  // const data = {
  //   "prompt": "你很好"
  // };
  // await axios.post('http://127.0.0.1:8088/claude/chat', data, {
  //   headers: {
  //     'Content-Type': 'application/json',
  //     'accept': 'application/json',
  //     'x-token': '13832bf5-ed7c-4b5c-a30b-e7db48d25b86'
  //   }
  // })
  //   .then((response) => {
  //     console.log("----------------");
  //     console.log(response.data);
  //     assistantMessage=response.data.claude as string;
  //     assistantMessage=assistantMessage.replace(/^\n+|\n+$/g, "");
  //     console.log("----------------");
  //   })
  //   .catch((error) => {
  //     console.log(error);
  //   });


    
  const initializedAt = Date.now()
  bot
    .on("scan", async (qrcode, status) => {
      const url = `https://wechaty.js.org/qrcode/${encodeURIComponent(qrcode)}`;
      console.log(`Scan QR Code to login: ${status}\n${url}`);
      console.log(
        await QRCode.toString(qrcode, { type: "terminal", small: true })
      );
    })
    .on("login", async (user) => {
      chatGPTBot.setBotName(user.name());
      console.log(`User ${user} logged in`);
      console.log(`私聊触发关键词: ${config.chatPrivateTriggerKeyword}`);
      console.log(`已设置 ${config.blockWords.length} 个聊天关键词屏蔽. ${config.blockWords}`);
      console.log(`已设置 ${config.chatgptBlockWords.length} 个ChatGPT回复关键词屏蔽. ${config.chatgptBlockWords}`);
    })
    .on("message", async (message) => {
      if (message.date().getTime() < initializedAt) {
        return;
      }
      if (message.text().startsWith("/ping")) {
        await message.say("pong");
        return;
      }
      try {
        await chatGPTBot.onMessage(message);
      } catch (e) {
        console.error(e);
      }
    });
  try {
    await bot.start();
  } catch (e) {
    console.error(
      `⚠️ Bot start failed, can you log in through wechat on the web?: ${e}`
    );
  }
}
main();
